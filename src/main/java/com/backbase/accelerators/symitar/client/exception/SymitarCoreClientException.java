package com.backbase.accelerators.symitar.client.exception;

public class SymitarCoreClientException extends RuntimeException {

    public SymitarCoreClientException(String message) {
        super(message);
    }
}
